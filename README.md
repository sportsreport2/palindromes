

## Table of Contents

- [Requirements](#requirements)
- [Getting Started](#getting-started)
- [Deviations From Requirements](#deviations-from-requirements)
- [Possible Improvements](#possible-improvements)

##Requirements

**Goal**

- Write a program which scans an input file with strings and outputs a file that lists the palindromes found from each line. 

**Output**

- An array of JSON objects. 
- Each object contains the following information:
    - The entire line, unmodified, within which one or more palindromes were found
    - The list of palindromes found on a given line
    - The total number of characters comprising all palindromes found on a given line, with punctuation and white-space removed
    - Order the list of lines/palindromes found in decreasing order of the number of characters within palindromes found for a given line, punctuation and white-space excluded
    - If a given line contains no palindromes, the line will not be in the output

## Deviations From Requirements
- Added a count of unique characters found in all palindromes for each line of the input file
- Added output of total number of palindromes after command
- Added a totals array to the JSON output containing the following totals
    - totalLinesProcessed
    - totalLinesContainingPalindrome
    - totalPalindromes
    - totalCharacters
    - totalUniqueCharacters
 
## Getting Started 
*All commands below assume your current working directory is this project

Dependencies

- PHP CLI >= 5.6

```sh
Usage:
php processPalindromes.php [-i <FILE>] [-o <FILE>]

Options:
  -i <FILE>    Input file name [default: palindromes.txt]
  -o <FILE>    Output file name [default: processedPalindromes.json]
```
 
*To update the file being processed or the output file name, change the following variables at the top of processPalindromes:
inputFileName - name of input file
outputFileName - name of output file
 
## Possible Improvements
- Use a more efficient algorithm for finding all palindromes
	- If the string is quite long looping through each character for each character could be inefficient
- Add case sensitive vs case insensitive option to allow for finding palindromes based on the respective case
- Add unit tests (PHP Unit)
- Add better error handling leveraging try/catch and exceptions
	- Would allow for more specific errors 
	- Easier to handle and understand
- Save totals in their own class 
	- Less totals code in main class
	- Easier to understand how totals are calculated
