<?php
//Get command line options
$commandLineOptions = getopt("i:o:");

//Define input file name and output file name
$inputFileName = (array_key_exists("i", $commandLineOptions) ? $commandLineOptions["i"] : "palindromes.txt");
$outputFileName = (array_key_exists("o", $commandLineOptions) ? $commandLineOptions["o"] : "processedPalindromes.json");

//Create process palindromes file class
$processPalindromeFile = new ProcessPalindromesFile($inputFileName);
//Process palindromes and save to file
$result = $processPalindromeFile->processAndSaveToFile($outputFileName);

//Output result
if(!$result)
{
    echo "Error processing or saving file.";
}
else
{
    echo "DONE - Total Palindromes: ".$processPalindromeFile->getTotalPalindromeCount();
}

echo "\n";


class ProcessPalindromesFile
{
    const PROCESS_WITH_2_LOOPS = "PROCESS_WITH_2_LOOPS";

    private $fileName = "";
    private $palindromes = [];
    private $processType = self::PROCESS_WITH_2_LOOPS;
    private $totals = array(
        'totalLinesProcessed' => 0,
        'totalLinesContainingPalindrome' => 0,
        'totalPalindromes' => 0,
        'totalCharacters' => 0,
        'totalUniqueCharacters' => 0
    );

    public function __construct($fileName, $processType = self::PROCESS_WITH_2_LOOPS)
    {
        $this->fileName = $fileName;
        $this->processType = $processType;
    }

    /**
     * This function combines the processing and saving of a palindromes file into one easily called function.
     * @param $fileName
     * @return bool
     */
    public function processAndSaveToFile($fileName)
    {
        $result = $this->processFile();
        if(!$result)
        {
            return false;
        }
        $result = $this->savePalindromesToFile($fileName);
        if(!$result)
        {
            return false;
        }
        return true;
    }

    /**
     * This function processes the the file based on the fileName given when this object was constructed.
     * Processing includes opening the file, looping through each line of the file, stripping unwanted characters,
     * finding palindromes in the given line, and saving those palindromes to an array.
     * @return bool
     */
    public function processFile()
    {
        if($this->fileName == "")
        {
            return false;
        }

        if(!file_exists($this->fileName))
        {
            return false;
        }

        $file = fopen($this->fileName, "r");

        if(!$file)
        {
            return false;
        }

        $lines = 0;
        $linesContainingPalindrome = 0;
        //Loop until end of file
        while(!feof($file))
        {
            //Get line contents
            $fileLine = fgets($file);
            //Strip all non-alphanumeric characters
            $fileLineAlphaNumericOnly = $this->stringToAlphaNumericOnly($fileLine);

            //Find palindromes
            $palindromes = $this->findPalindromesInString($fileLineAlphaNumericOnly);
            //Only add if palindromes found
            if(count($palindromes) > 0)
            {
                //Save info
                $this->addPalindrome($fileLine, $fileLineAlphaNumericOnly, $palindromes);
                $linesContainingPalindrome++;
            }

            $lines++;
        }

        //Save totals
        $this->totals['totalLinesProcessed'] = $lines;
        $this->totals['totalLinesContainingPalindrome'] = $linesContainingPalindrome;
        $this->totals['totalPalindromes'] = $this->getTotalPalindromeCount();
        $this->totals['totalCharacters'] = $this->getTotalCharactersInPalindromes();
        $this->totals['totalUniqueCharacters'] = $this->getTotalUniqueCharactersInPalindromes();

        return true;
    }

    /**
     * This function json_encodes the palindromes array and saves it to a file with the given fileName.
     * @param $fileName
     * @return bool
     */
    public function savePalindromesToFile($fileName)
    {
        if(!file_exists($fileName))
        {
            return false;
        }

        $output = array(
            'totals' => $this->totals,
            'output' => $this->palindromes
        );
        file_put_contents($fileName, json_encode($output));
        return true;
    }

    /**
     * This function calls the proper processing method based on the process type.
     * Currently there is only one processing method: findPalindromesWith2Loops
     * @param $string
     * @return array
     */
    public function findPalindromesInString($string)
    {
        if($this->processType == self::PROCESS_WITH_2_LOOPS)
        {
            return $this->findPalindromesWith2Loops($string);
        }

        return [];
    }

    /**
     * This function processes a string to find palindromes.
     * This method is looping through each character from left to right.
     * Then from right to left for each character in the first loop.
     * The goal is to find all palindromes in the given string at every given point in the string.
     * When a palindrome is found, it is added to an array. Then that array is returned.
     * @param $string
     * @return array
     */
    public function findPalindromesWith2Loops($string)
    {
        $palindromes = [];
        $length = strlen($string);

        //Loop through string starting from left until end
        for ($left = 0; $left < $length - 1; $left++) {
            //Loop through string starting from right until left pointer
            for ($right = $length - $left; $right > 1; $right--) {
                //Create substring starting at $left to $right
                $s = substr($string, $left, $right);
                //Check if substring is palindrome
                if ($this->isPalindrome($s)) {
                    //if palindrome, add as new palindrome
                    array_push($palindromes, $s);
                }
            }
        }

        return $palindromes;
    }

    /**
     * This function returns the total number of palindromes found in the input file.
     * @return int
     */
    public function getTotalPalindromeCount()
    {
        $a = array_column($this->palindromes, "palindromes");
        return array_sum(array_map("count", $a));
    }

    /**
     * This function returns the total number of characters in palindromes found in input file.
     * @return int
     */
    public function getTotalCharactersInPalindromes()
    {
        return $this->getTotalFromArray($this->palindromes, "totalCharactersInPalindromes");
    }

    public function getTotalUniqueCharactersInPalindromes()
    {
        return $this->getTotalFromArray($this->palindromes, "totalUniqueCharactersInPalindromes");
    }

    /**
     * This function creates the proper output entry for a given line in the input file.
     * This allows one to control what the format of the output is.
     * @param $originalFileLine
     * @param $processedFileLine
     * @param $palindromes
     */
    private function addPalindrome($originalFileLine, $processedFileLine, $palindromes)
    {
        //Sort palindromes by string length desc
        $palindromes = $this->sortArrayByStringLength($palindromes, "desc");
        //Get character counts (totalCharacters and totalUniqueCharacters)
        $characterCounts = $this->getCharacterCounts($palindromes);

        array_push($this->palindromes, array(
            'originalFileLine' => $originalFileLine,
            'processedFileLine' => $processedFileLine,
            'totalCharactersInPalindromes' => $characterCounts['totalCharacters'],
            'totalUniqueCharactersInPalindromes' => $characterCounts['totalUniqueCharacters'],
            'palindromes' => $palindromes
        ));
    }

    /**
     * Get the palindromes array.
     * @return array
     */
    public function getPalindromes()
    {
        return $this->palindromes;
    }

    /**
     * Get the file name.
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set the file name.
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Get the process
     * @return string
     */
    public function getProcessType()
    {
        return $this->processType;
    }

    /**
     * @param string $processType
     */
    public function setProcessType($processType)
    {
        $this->processType = $processType;
    }

    /**
     * This function takes a string parameter and replaces all non alphanumeric characters with blank.
     * @param $string
     * @return string
     */
    private function stringToAlphaNumericOnly($string)
    {
        return preg_replace('/[^ \w]+/', "", $string);
    }

    /**
     * This function takes a string parameter and checks to see if it is a palindrome.
     * A palindrome is defined as a word, phrase, or sequence that reads the same backward as forward.
     * To check this, compare the string parameter to its reverse.
     * @param $string
     * @return bool
     */
    private function isPalindrome($string) {
        return $string == strrev($string);
    }

    /**
     * This function sorts an array by string length.
     * The parameter direction determines ascending or descending.
     * @param $stringArray array
     * @param $direction 'asc'|'desc'
     * @return mixed
     */
    private function sortArrayByStringLength($stringArray, $direction)
    {
        usort($stringArray, function($a, $b) use ($direction){
            $a = strlen($a);
            $b = strlen($b);
            if($a==$b)
                return 0;
            return ($a<$b) ? ($direction == "asc" ? -1 : 1) : ($direction == "asc" ? 1 : -1);
        });
        return $stringArray;
    }

    /**
     * This function determines the number of total characters and total unique characters present in a string array.
     * @param $stringArray
     * @return array
     */
    private function getCharacterCounts($stringArray)
    {
        $totalCharacters = 0;
        $totalUniqueCharacters = 0;
        foreach($stringArray as $string)
        {
            $totalCharacters += strlen($string);
            $totalUniqueCharacters += count(array_unique(str_split($string)));
        }

        return array(
            'totalCharacters' => $totalCharacters,
            'totalUniqueCharacters' => $totalUniqueCharacters
        );
    }

    /**
     * This function takes a multidimensional array and totals the values found at key.
     * Ex. $a = array(
     *      array('test' => 10),
     *      array('test' => 20)
     * )
     * If getTotalFromArray($a, 'test') is called, 30 would be returned.
     * @param $key
     * @return int
     */
    private function getTotalFromArray($array, $key)
    {
        $a = array_column($array, $key);
        return array_sum($a);

//        $total = 0;
//        foreach($array as $value)
//        {
//            $total += count($value[$key]);
//        }
//        return $total;
    }
}